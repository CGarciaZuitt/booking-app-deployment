// import { useState, useEffect } from 'react';
import { Card, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CourseCard(props) {

    //object destructuring
    const {breakpoint, courseProp} = props
    const {name, description, price, _id} = courseProp
    // console.log(name);

    return (
        <Col xs={12} md={breakpoint} className="mt-4">
        <Card className="card1">
            <Card.Body>
                <Card.Title className="text-center card2">{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text className="card3">{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Link className="btn btn-primary" to={`/courseView/${_id}`}>View Details</Link>
            </Card.Body>
        </Card>
        </Col>
   
    )
}
