import {Row} from 'react-bootstrap'
import { useEffect, useState } from 'react'
import CourseCard from '../components/CourseCard';

export default function Courses({course}){

	const [courses, setCourses] = useState([])

	useEffect(() => {
		fetch('https://cryptic-scrubland-63460.herokuapp.com/courses')
		.then(res => res.json())
		.then(data => {
			console.log(data)

			const coursesArr = (data.map(course => {

				return (
					<CourseCard key={course._id} courseProp={course} breakpoint={4} />
				)
				
			}))
			setCourses(coursesArr)
		})

	}, [course])

	return(
	<>
		<Row>
		<h1> Courses Available: </h1>
			{courses}
		</Row>

	</>
	)
}

 